# S2 Light Map
This repository contains the code required to create and validate the S2 optical light map used in BaccMCTruth.
More information is in the software docs.

## Installation and Usage

Step 1: Get files

`git clone git@lz-git.ua.edu:seriksen/S2LightMap.git; cd S2LightMap`

Step 2: Compile 

`./compile.sh`

Step 3: Run a light bomb simulation and create hit map

`./runProduceS2HitMaps.sh`

Step 4: Create light map

`./produceLCES2HitMaps.sh outputs/output.root`

## The files
`compile.sh` uses g++ compiler to compile the two c++ scripts required to produce the light map: 
* produceS2HitMaps
* produceS2LCEMaps

`produceS2HitMaps.cc` takes the output of `BaccRootConverter`, ie a .root file containing a Header and Data tree.
It outputs 3 groups of histograms:
* Start position of photons - stored as xyz
* Start position of photons which hit PMTs (one for each PMT and one containing every time) - stored as xyz
* Time histograms (the distance the photon started from the PMT it hit and the time it took to hit the PMT) - stored as r,t

`produceS2LCEMaps.cc` takes the output from `produceS2HitMaps`, ie a .root file containing a number of histograms.
The output of this file is the light map used in BaccMCTruth.
It outputs 3 groups of histograms:
* PMT Light Collection Efficiency (one for each PMT)
* Cumulative time probability of a photon to hit a PMT from a given distance
* Probability of a photon to hit a PMT from a given distance in a given time

`runProduceS2HitMaps.sh` is a bash and slurm script. It initiates the BACCARAT simulation, Root conversion and Hit Map production.

`S2LightCollectionBath.mac` Macro used with BACCARAT. Emits photons in Xenon gas volume (often referred to as photon bomb) 


## Additional Files (for Validation)
`readSlurmLog.py` If Slurm has been used for batch job submission, then this will read the log files from that and extract the time it took for BACCARAT to run
about for the time histograms.

`haddS2HitMaps.sh` Combine a number of histograms (the hit maps) into one larger file

`changeS2HitMapBinngin.cc` Change the number of z-bins in the hit maps

### Validation
Some files used for validation work/converting root histograms into txt files for latex plotting
