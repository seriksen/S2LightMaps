#!/bin/bash
#
####################
## SLURM Settings ##
####################
#SBATCH --job-name=S2LightCollection
#SBATCH --array=1-55
#SBATCH --time=4:00:00
#SBATCH --nodes=1
#SBATCH --mem-per-cpu=3G
#SBATCH --ntasks=1
#SBATCH --account=lz
#SBATCH --output=logs/slurm_%a.out
#SBATCH --error=logs/slurm_%a.err
###############################
#
# This script combines the histograms contained in the root files which are the output of produceS2HitMaps.
#
#######################
## Variable Values   ##
#######################
#
# Oracle OCI
SOURCE_DIR=/mnt/shared/home/sam/S2LightMap
OUTPUT_DIR=${SOURCE_DIR}/total_hits
BACC_DIR=/mnt/shared/home/sam/BACCARAT
LZ_BUILD_CONFIG=x86_64-centos7-gcc7-opt
#
# NERSC
#SOURCE_DIR=/global/projecta/projectdirs/lz/data/bristol/s2maps/sam/S2LightMap/outputs/hits
#OUTPUT_DIR=${SOURCE_DIR}/total_hits
#BACC_DIR=/global/projecta/projectdirs/lz/data/bristol/s2maps/sam/BACCARAT/
#LZ_BUILD_CONFIG=x86_64-slc6-gcc48-opt
#
# If using slurm
arrayNum=$SLURM_ARRAY_TASK_ID
nFiles=100 # + 1
a=$((${arrayNum}-1))
b=$((${a}*${nFiles}))
startFile=$((${b}+1))
endFile=$((${arrayNum}*${nFiles}))
#
# If not using slurm
#startFile=1
#endFile=100
#
#
# FileNames
fileName="S2Photons"
outFileName="total_hits_${startFile}_to_${endFile}.root"
#
# Source
source ${BACC_DIR}/setup.sh
#
################
## Hadd Files ##
################
echo "Hadding to" ${outFileName}

cp ${SOURCE_DIR}/${fileName}_${startFile}_hits.root ${outFileName}

echo ${startFile}
echo ${endFile}
#
for i in `seq ${startFile} ${endFile}`;
do
mv ${outFileName} totalTemp_${startFile}.root
hadd ${outFileName} totalTemp_${startFile}.root ${fileName}_${i}_hits.root
rm totalTemp_${startFile}.root
done

mv ${outFileName} ${OUTPUT_DIR}/${outFileName}


