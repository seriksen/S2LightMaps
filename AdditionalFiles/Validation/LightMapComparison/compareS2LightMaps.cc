/*
 * Filename:
 * compareS2LightMaps.cc
 *
 * Description:
 *
 * Compile Instructions:
 * source BACC_DIR/setup.sh
 * g++ compareS2LightMaps.cc -o compareS2LightMaps `root-config --cflags --glibs`
 *
 * Script Inputs:
 * - .root file, eg an official S2Lightmap
 * - .root file, eg your S2LightMap
 *
 *
 */

// C++/C includes
#include <stdio.h>
#include <iostream>

// ROOT includes
#include <TH3F.h>
#include <TH3I.h>
#include <TH2F.h>
#include <TH2I.h>
#include <TAxis.h>
#include <TFile.h>
#include <TTree.h>

void fillComparisonHist(TH2F* &file1_2F, TH3F* &file1_3F,
                        TH2F* &file2Hist, TH3F* &file2_3F,
                        TH2F* &difference_2F,
                        double nBinsX, double nBinsY, double nBinsZ);


void fillPosMeans(TH1F* &file1xMean, TH1F* &file1yMean, TH1F* &file1zMean,
               TH1F* &file2xMean, TH1F* &file2yMean, TH1F* &file2zMean,
               TH1F* &differencexMean, TH1F* &differenceyMean, TH1F* &differencezMean,
               TH3F* &file1_3F, TH3F* &file2_3F, int thisbin);


int main(int argc, char* argv[]){

    /***************
     * Check input *
     ***************/
    try{
        if(argc<3) throw "Not enough filenames specified";
        // Check first map
        std::string file1Name = std::string(argv[1]);
        file1Name = file1Name.substr(file1Name.length()-5,5);
        if(file1Name != ".root") throw "File 1 not a ROOT file";
        TFile* file1 = new TFile(argv[1]);
        if(file1->IsZombie()) throw "Error opening file 1";
        // Check second map
        std::string file2Name = std::string(argv[2]);
        file2Name = file2Name.substr(file2Name.length()-5,5);
        if(file2Name != ".root") throw "File 2 not a ROOT file";
        TFile* file2 = new TFile(argv[2]);
        if(file2->IsZombie()) throw "Error opening file 2";
    }
    catch(const char* msg){
        std::cerr << msg << " - compareS2LightMaps will exit" << std::endl;
        return 1;
    }

    std::string outFileName = "S2MapComparison.root";

    /**************
     * Read Files *
     **************/
    std::cout << "\n\n"
              << "File 1: " << argv[1] << "\n"
              << "File 2: " << argv[2] << "\n"
              << "OutFile: " << outFileName << "\n"
              << std::endl;

    // File 1
    TFile* file1 = new TFile(argv[1],"read");

    // File 2
    TFile* file2 = new TFile(argv[2],"read");

    /*****************************
     * Setup 2D comparison Maps *
     *****************************/
    size_t noOfMaps = file1->GetListOfKeys()->GetSize();

    // Get bin sizes from totalHitProb
    TH3F* file1_totalHitProb_3F = (TH3F*)file1->Get("totalHitProb");

    const int nBinsX{file1_totalHitProb_3F->GetNbinsX()};
    const int nBinsY{file1_totalHitProb_3F->GetNbinsY()};
    const int nBinsZ{file1_totalHitProb_3F->GetNbinsZ()};

    const double xMin{file1_totalHitProb_3F->GetXaxis()->GetBinLowEdge(1)};
    const double xMax{file1_totalHitProb_3F->GetXaxis()->GetBinUpEdge(nBinsX)};
    const double yMin{file1_totalHitProb_3F->GetYaxis()->GetBinLowEdge(1)};
    const double yMax{file1_totalHitProb_3F->GetYaxis()->GetBinUpEdge(nBinsY)};


    /************************
     * Create and fill maps *
     ************************/

    std::vector<TH2F*> lceDifferenceMaps;
    size_t noOfLCEMaps{0};

    // Create histograms for means

    // Top Array
    // PMT top array starts at 000 and goes up to 252
    int topMapStartNum{0};
    int noOfTopMaps{252-topMapStartNum};

    TH1F* topPMT_xMean_file1 = new TH1F("topPMT_xMean_file1", "topPMT_xMean_file1", noOfTopMaps, topMapStartNum, noOfTopMaps+topMapStartNum);
    TH1F* topPMT_yMean_file1 = new TH1F("topPMT_yMean_file1", "topPMT_yMean_file1", noOfTopMaps, topMapStartNum, noOfTopMaps+topMapStartNum);
    TH1F* topPMT_zMean_file1 = new TH1F("topPMT_zMean_file1", "topPMT_zMean_file1", noOfTopMaps, topMapStartNum, noOfTopMaps+topMapStartNum);
    topPMT_xMean_file1->GetXaxis()->SetTitle("PMT number");
    topPMT_xMean_file1->GetYaxis()->SetTitle("Mean (mm)");
    topPMT_yMean_file1->GetXaxis()->SetTitle("PMT number");
    topPMT_yMean_file1->GetYaxis()->SetTitle("Mean (mm)");
    topPMT_zMean_file1->GetXaxis()->SetTitle("PMT number");
    topPMT_zMean_file1->GetYaxis()->SetTitle("Mean (mm)");

    TH1F* topPMT_xMean_file2 = new TH1F("topPMT_xMean_file2", "topPMT_xMean_file2", noOfTopMaps, topMapStartNum, noOfTopMaps+topMapStartNum);
    TH1F* topPMT_yMean_file2 = new TH1F("topPMT_yMean_file2", "topPMT_yMean_file2", noOfTopMaps, topMapStartNum, noOfTopMaps+topMapStartNum);
    TH1F* topPMT_zMean_file2 = new TH1F("topPMT_zMean_file2", "topPMT_zMean_file2", noOfTopMaps, topMapStartNum, noOfTopMaps+topMapStartNum);
    topPMT_xMean_file2->GetXaxis()->SetTitle("PMT number");
    topPMT_xMean_file2->GetYaxis()->SetTitle("Mean (mm)");
    topPMT_yMean_file2->GetXaxis()->SetTitle("PMT number");
    topPMT_yMean_file2->GetYaxis()->SetTitle("Mean (mm)");
    topPMT_zMean_file2->GetXaxis()->SetTitle("PMT number");
    topPMT_zMean_file2->GetYaxis()->SetTitle("Mean (mm)");

    TH1F* topPMT_xMean_difference = new TH1F("topPMT_xMean_difference", "topPMT_xMean_difference", noOfTopMaps, topMapStartNum, noOfTopMaps+topMapStartNum);
    TH1F* topPMT_yMean_difference = new TH1F("topPMT_yMean_difference", "topPMT_yMean_difference", noOfTopMaps, topMapStartNum, noOfTopMaps+topMapStartNum);
    TH1F* topPMT_zMean_difference = new TH1F("topPMT_zMean_difference", "topPMT_zMean_difference", noOfTopMaps, topMapStartNum, noOfTopMaps+topMapStartNum);
    topPMT_xMean_difference->GetXaxis()->SetTitle("PMT number");
    topPMT_xMean_difference->GetYaxis()->SetTitle("Difference (mm)");
    topPMT_yMean_difference->GetXaxis()->SetTitle("PMT number");
    topPMT_yMean_difference->GetYaxis()->SetTitle("Difference (mm)");
    topPMT_zMean_difference->GetXaxis()->SetTitle("PMT number");
    topPMT_zMean_difference->GetYaxis()->SetTitle("Difference (mm)");

    // Bottom Array
    // PMT bottom array starts at 300 and goes up to 540
    size_t bottomMapStartNum{300};
    size_t noOfBottomMaps{540-bottomMapStartNum};

    TH1F* bottomPMT_xMean_file1 = new TH1F("bottomPMT_xMean_file1", "bottomPMT_xMean_file1", noOfBottomMaps, bottomMapStartNum, noOfBottomMaps+bottomMapStartNum);
    TH1F* bottomPMT_yMean_file1 = new TH1F("bottomPMT_yMean_file1", "bottomPMT_yMean_file1", noOfBottomMaps, bottomMapStartNum, noOfBottomMaps+bottomMapStartNum);
    TH1F* bottomPMT_zMean_file1 = new TH1F("bottomPMT_zMean_file1", "bottomPMT_zMean_file1", noOfBottomMaps, bottomMapStartNum, noOfBottomMaps+bottomMapStartNum);
    bottomPMT_xMean_file1->GetXaxis()->SetTitle("PMT number");
    bottomPMT_xMean_file1->GetYaxis()->SetTitle("Mean (mm)");
    bottomPMT_yMean_file1->GetXaxis()->SetTitle("PMT number");
    bottomPMT_yMean_file1->GetYaxis()->SetTitle("Mean (mm)");
    bottomPMT_zMean_file1->GetXaxis()->SetTitle("PMT number");
    bottomPMT_zMean_file1->GetYaxis()->SetTitle("Mean (mm)");
    TH1F* bottomPMT_xMean_file2 = new TH1F("bottomPMT_xMean_file2", "bottomPMT_xMean_file2", noOfBottomMaps, bottomMapStartNum, noOfBottomMaps+bottomMapStartNum);
    TH1F* bottomPMT_yMean_file2 = new TH1F("bottomPMT_yMean_file2", "bottomPMT_yMean_file2", noOfBottomMaps, bottomMapStartNum, noOfBottomMaps+bottomMapStartNum);
    TH1F* bottomPMT_zMean_file2 = new TH1F("bottomPMT_zMean_file2", "bottomPMT_zMean_file2", noOfBottomMaps, bottomMapStartNum, noOfBottomMaps+bottomMapStartNum);
    bottomPMT_xMean_file2->GetXaxis()->SetTitle("PMT number");
    bottomPMT_xMean_file2->GetYaxis()->SetTitle("Mean (mm)");
    bottomPMT_yMean_file2->GetXaxis()->SetTitle("PMT number");
    bottomPMT_yMean_file2->GetYaxis()->SetTitle("Mean (mm)");
    bottomPMT_zMean_file2->GetXaxis()->SetTitle("PMT number");
    bottomPMT_zMean_file2->GetYaxis()->SetTitle("Mean (mm)");
    TH1F* bottomPMT_xMean_difference = new TH1F("bottomPMT_xMean_difference", "bottomPMT_xMean_difference", noOfBottomMaps, bottomMapStartNum, noOfBottomMaps+bottomMapStartNum);
    TH1F* bottomPMT_yMean_difference = new TH1F("bottomPMT_yMean_difference", "bottomPMT_yMean_difference", noOfBottomMaps, bottomMapStartNum, noOfBottomMaps+bottomMapStartNum);
    TH1F* bottomPMT_zMean_difference = new TH1F("bottomPMT_zMean_difference", "bottomPMT_zMean_difference", noOfBottomMaps, bottomMapStartNum, noOfBottomMaps+bottomMapStartNum);
    bottomPMT_xMean_difference->GetXaxis()->SetTitle("PMT number");
    bottomPMT_xMean_difference->GetYaxis()->SetTitle("Difference (mm)");
    bottomPMT_yMean_difference->GetXaxis()->SetTitle("PMT number");
    bottomPMT_yMean_difference->GetYaxis()->SetTitle("Difference (mm)");
    bottomPMT_zMean_difference->GetXaxis()->SetTitle("PMT number");
    bottomPMT_zMean_difference->GetYaxis()->SetTitle("Difference (mm)");


    int topCounter{0};
    int bottomCounter{0};

    for(size_t i = 0; i<noOfMaps; ++i){
        if(i==0){
            TH3F* file1_3F = (TH3F*)file1->Get("totalHitProb");
            TH3F* file2_3F = (TH3F*)file2->Get("totalHitProb");
            TH2F* file1_2F = new TH2F("file1_totalHitProb", "file1_totalHitProb",
                                      nBinsX, xMin, xMax,
                                      nBinsY, yMin, yMax);
            file1_2F->GetXaxis()->SetTitle("x (mm)");
            file1_2F->GetYaxis()->SetTitle("y (mm)");
            TH2F* file2_2F = new TH2F("file2_totalHitProb", "file2_totalHitProb",
                                      nBinsX, xMin, xMax,
                                      nBinsY, yMin, yMax);
            file2_2F->GetXaxis()->SetTitle("x (mm)");
            file2_2F->GetYaxis()->SetTitle("y (mm)");
            TH2F* differenceMap = new TH2F("totalHitProb_Difference", "totalHitProb_Difference",
                                           nBinsX, xMin, xMax,
                                           nBinsY, yMin, yMax);
            differenceMap->GetXaxis()->SetTitle("x (mm)");
            differenceMap->GetYaxis()->SetTitle("y (mm)");
            fillComparisonHist(file1_2F, file1_3F, file2_2F, file2_3F, differenceMap, nBinsX, nBinsY, nBinsZ);

            lceDifferenceMaps.push_back(differenceMap);
            ++noOfLCEMaps;
        }

        std::string mapName = std::string(file1->GetListOfKeys()->At(i)->GetName());

        if(mapName.find("PMT") != std::string::npos){
            TH3F* file1_3F = (TH3F*)file1->Get(mapName.c_str());
            TH3F* file2_3F = (TH3F*)file2->Get(mapName.c_str());

            std::string file1mapname = "file1_"; file1mapname+=mapName.c_str();
            std::string file2mapname = "file2_"; file2mapname+=mapName.c_str();

            TH2F* file1_2F = new TH2F(file1mapname.c_str(), file1mapname.c_str(),
                                      nBinsX, xMin, xMax,
                                      nBinsY, yMin, yMax);
            file1_2F->GetXaxis()->SetTitle("x (mm)");
            file1_2F->GetYaxis()->SetTitle("y (mm)");
            TH2F* file2_2F = new TH2F(file2mapname.c_str(), file2mapname.c_str(),
                                      nBinsX, xMin, xMax,
                                      nBinsY, yMin, yMax);
            file2_2F->GetXaxis()->SetTitle("x (mm)");
            file2_2F->GetYaxis()->SetTitle("y (mm)");
            mapName+="_difference";
            TH2F* differenceMap = new TH2F(mapName.c_str(), mapName.c_str(),
                                           nBinsX, xMin, xMax,
                                           nBinsY, yMin, yMax);
            differenceMap->GetXaxis()->SetTitle("x (mm)");
            differenceMap->GetYaxis()->SetTitle("y (mm)");
            fillComparisonHist(file1_2F, file1_3F, file2_2F, file2_3F, differenceMap, nBinsX, nBinsY, nBinsZ);

            lceDifferenceMaps.push_back(differenceMap);
            ++noOfLCEMaps;

            // Determine if PMT belongs to top or bottom PMT array

            if(mapName.find("Top") != std::string::npos){

                fillPosMeans(topPMT_xMean_file1, topPMT_yMean_file1, topPMT_zMean_file1,
                        topPMT_xMean_file2, topPMT_yMean_file2, topPMT_zMean_file2,
                        topPMT_xMean_difference, topPMT_yMean_difference, topPMT_zMean_difference,
                        file1_3F, file2_3F, topCounter);
                ++topCounter;
            }
            else {
                fillPosMeans(bottomPMT_xMean_file1, bottomPMT_yMean_file1, bottomPMT_zMean_file1,
                        bottomPMT_xMean_file2, bottomPMT_yMean_file2, bottomPMT_zMean_file2,
                        bottomPMT_xMean_difference, bottomPMT_yMean_difference, bottomPMT_zMean_difference,
                        file1_3F, file2_3F, bottomCounter);
                ++bottomCounter;
            }
        }
    }


    /*****************************
     * Tidy up and write outputs *
     *****************************/
    TFile* outfile = new TFile(outFileName.c_str(), "recreate");

    std::cout << "Writting out difference maps" << std::endl;
    for (size_t i = 0; i<noOfLCEMaps; ++i){
        lceDifferenceMaps[i]->Write(lceDifferenceMaps[i]->GetName());
        delete lceDifferenceMaps[i];
    }

    std::cout << "Writting out mean histograms" << std::endl;

    topPMT_xMean_file1->Write(topPMT_xMean_file1->GetName());
    topPMT_yMean_file1->Write(topPMT_yMean_file1->GetName());
    topPMT_zMean_file1->Write(topPMT_zMean_file1->GetName());
    delete topPMT_xMean_file1; delete topPMT_yMean_file1; delete topPMT_zMean_file1;
    topPMT_xMean_file2->Write(topPMT_xMean_file2->GetName());
    topPMT_yMean_file2->Write(topPMT_yMean_file2->GetName());
    topPMT_zMean_file2->Write(topPMT_zMean_file2->GetName());
    delete topPMT_xMean_file2; delete topPMT_yMean_file2; delete topPMT_zMean_file2;
    topPMT_xMean_difference->Write(topPMT_xMean_difference->GetName());
    topPMT_yMean_difference->Write(topPMT_yMean_difference->GetName());
    topPMT_zMean_difference->Write(topPMT_zMean_difference->GetName());
    delete topPMT_xMean_difference; delete topPMT_yMean_difference; delete topPMT_zMean_difference;
    bottomPMT_xMean_file1->Write(bottomPMT_xMean_file1->GetName());
    bottomPMT_yMean_file1->Write(bottomPMT_yMean_file1->GetName());
    bottomPMT_zMean_file1->Write(bottomPMT_zMean_file1->GetName());
    delete bottomPMT_xMean_file1; delete bottomPMT_yMean_file1; delete bottomPMT_zMean_file1;
    bottomPMT_xMean_file2->Write(bottomPMT_xMean_file2->GetName());
    bottomPMT_yMean_file2->Write(bottomPMT_yMean_file2->GetName());
    bottomPMT_zMean_file2->Write(bottomPMT_zMean_file2->GetName());
    delete bottomPMT_xMean_file2; delete bottomPMT_yMean_file2; delete bottomPMT_zMean_file2;
    bottomPMT_xMean_difference->Write(bottomPMT_xMean_difference->GetName());
    bottomPMT_yMean_difference->Write(bottomPMT_yMean_difference->GetName());
    bottomPMT_zMean_difference->Write(bottomPMT_zMean_difference->GetName());
    delete bottomPMT_xMean_difference; delete bottomPMT_yMean_difference; delete bottomPMT_zMean_difference;

    outfile->Close();

    delete outfile;
    return 0;
}


void fillComparisonHist (TH2F* &file1_2F, TH3F* &file1_3F,
                         TH2F* &file2_2F, TH3F* &file2_3F,
                         TH2F* &difference_2F,
                         double nBinsX, double nBinsY, double nBinsZ)
{
/**
 * This function converts TH3F into TH2F for file1 and file2 and then compared the two
 * The probabilities in z are added up for each x-y bin and the comparison is the difference between the two
 *
 * \param file1_2F TH2F to be filled
 * \param file1_3F TH3F to be flattened
 * \param file2_2F TH2F to be filled
 * \param file2_3F TH3F to be flattened
 * \param difference_2F TH2F to be filled with the difference between file1Hist and file2Hist
 * \param nBinsX number of x bins in the TH3F
 * \param nBinsY number of y bins in the TH3F
 * \param nBinsZ number of z bins in the TH3F
 **/

    double file1BinContents_thisLoop{0};
    double file1BinContents_z{0};

    double file2BinContents_thisLoop{0};
    double file2BinContents_z{0};

    // Loop over x and y bins
    for(int i = 1; i<nBinsX; ++i){
        for(int j = 1; j<nBinsY; ++j){

            file1BinContents_z = file2BinContents_z = 0;

            // Loop over z bins as this is the one we are flattening
            for(int k = 1; k<nBinsZ; ++k){

                // file 1
                file1BinContents_thisLoop = file1_3F->GetBinContent(i,j,k);
                file1BinContents_z += file1BinContents_thisLoop;
                //file1LCE += file1BinContents_thisLoop;

                // file 2
                file2BinContents_thisLoop = file2_3F->GetBinContent(i,j,k);
                file2BinContents_z += file2BinContents_thisLoop;
                //file2LCE += file2BinContents_thisLoop;

            } // z bins

            // Divide by number of z-bins
            file1BinContents_z /= nBinsZ;
            file2BinContents_z /= nBinsZ;

            // fill maps
            file1_2F->SetBinContent(i,j,file1BinContents_z);
            file2_2F->SetBinContent(i,j,file2BinContents_z);

            // fill the comparison map
            if(file1BinContents_z != 0 || file2BinContents_z != 0){
                difference_2F->SetBinContent(i,j,file1BinContents_z-file2BinContents_z);
            }
            else{
                difference_2F->SetBinContent(i,j,-999);
            }

        } // y bins
    } // x bins
}
void fillPosMeans(TH1F* &file1xMean, TH1F* &file1yMean, TH1F* &file1zMean,
               TH1F* &file2xMean, TH1F* &file2yMean, TH1F* &file2zMean,
               TH1F* &differencexMean, TH1F* &differenceyMean, TH1F* &differencezMean,
               TH3F* &file1_3F, TH3F* &file2_3F, int thisbin)

{
/**
 * This function converts TH3F into TH2F for file1 and file2 and then compared the two
 * The probabilities in z are added up for each x-y bin and the comparison is the difference between the two
 *
 * \param file1xMean to be filled
 * \param file1yMean to be filled
 * \param file2zMean to be filled
 * \param file2xMean to be filled
 * \param file2yMean to be filled
 * \param file2zMean to be filled with mean of z bin
 * \param differencexMean difference in x means
 * \param differenceyMean difference in y means
 * \param differencezMean difference in z means
 * \param file1_3F histogram to be probed (for mean)
 * \param file2_3F histogram to be probed (for mean)
 * \param thisbin the bin for the data to go in
 **/
    file1xMean->SetBinContent(thisbin, file1_3F->GetMean(1));
    file1yMean->SetBinContent(thisbin, file1_3F->GetMean(2));
    file1zMean->SetBinContent(thisbin, file1_3F->GetMean(3));

    file2xMean->SetBinContent(thisbin, file2_3F->GetMean(1));
    file2yMean->SetBinContent(thisbin, file2_3F->GetMean(2));
    file2zMean->SetBinContent(thisbin, file2_3F->GetMean(3));

    differencexMean->SetBinContent(thisbin, file1_3F->GetMean(1)-file2_3F->GetMean(1));
    differenceyMean->SetBinContent(thisbin, file1_3F->GetMean(2)-file2_3F->GetMean(2));
    differencezMean->SetBinContent(thisbin, file1_3F->GetMean(3)-file2_3F->GetMean(3));
}
