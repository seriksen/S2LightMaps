"""
Filename:
checkPMTCumulativeProbability.py

Description:
Get the difference between absolute probability and relative probability between two light maps
Get the probability of hitting each PMT of two light maps

"""

# Imports
import matplotlib.pyplot as plt
import ROOT
import numpy as np
from AdditionalFiles.Validation.PMT_Locations import topArrayRs, bottomArrayRs, topArrayPolar, bottomArrayPolar

def get_pmt_hit_prob_from_bin(rootfile, bins=[100,100,3]):

    hit_probabilities = []
    # top pmts
    for i in np.arange(0,253):
        this_pmt_name = "Top_PMT_Photocathode_{0:0=3d}_I".format(i)
        print("Reading {0}".format(this_pmt_name))
        if i == 0:
            pmt_hit_prob = rootfile.Get(str(this_pmt_name)).GetBinContent(bins[0],bins[1],bins[2])
        else:
            prev_pmt_name = "Top_PMT_Photocathode_{0:0=3d}_I".format(i-1)
            pmt_hit_prob = rootfile.Get(str(this_pmt_name)).GetBinContent(bins[0], bins[1], bins[2]) - \
                           rootfile.Get(str(prev_pmt_name)).GetBinContent(bins[0], bins[1], bins[2])

        hit_probabilities.append(pmt_hit_prob)

    # fill middle numbers with zeros
    for i in np.arange(253,300):
        hit_probabilities.append(0.0)

    # bottom pmts
    for i in np.arange(300,541):
        this_pmt_name = "Bottom_PMT_Photocathode_{0:0=3d}_I".format(i)
        print("Reading {0}".format(this_pmt_name))

        if i == 300:
            prev_pmt_name = "Top_PMT_Photocathode_{0:0=3d}_I".format(252)
        else:
            prev_pmt_name  = "Bottom_PMT_Photocathode_{0:0=3d}_I".format(i-1)

        pmt_hit_prob = rootfile.Get(str(this_pmt_name)).GetBinContent(bins[0], bins[1], bins[2]) - \
                           rootfile.Get(str(prev_pmt_name)).GetBinContent(bins[0], bins[1], bins[2])
        hit_probabilities.append(pmt_hit_prob)

    return hit_probabilities

def compare_maps_pmt_hit_prob_from_bin(oldmap, newmap, saveoutputs=False):

    oldmap_pmt_prob = get_pmt_hit_prob_from_bin(oldmap)
    newmap_pmt_prob = get_pmt_hit_prob_from_bin(newmap)


    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.plot(oldmap_pmt_prob, label="oldmap")
    ax.plot(newmap_pmt_prob, label="newmap")
    ax.set_xlabel("pmt number")
    ax.set_ylabel("probabilitiy")
    ax.set_title("P(hit pmt given pmt will be hit)")
    plt.legend()
    plt.show()

    if saveoutputs:
        outfile = open('/home/ak18773/Documents/LZ/S2LightMaps/newLightMap/pmt_hit_prob.txt', "w")
        outfile.write("pmt \t oldmap \t newmap \n")

        for i in np.arange(np.size(oldmap_pmt_prob)):
            outfile.write("{0} \t {1} \t {2} \n".format(i, oldmap_pmt_prob[i], newmap_pmt_prob[i]))
        outfile.close()
        outfile = open('/home/ak18773/Documents/LZ/S2LightMaps/newLightMap/pmt_hit_prob_difference.txt', "w")
        outfile.write("pmt \t difference \n")

        for i in np.arange(np.size(oldmap_pmt_prob)):
            outfile.write("{0} \t {1} \n".format(i, oldmap_pmt_prob[i]-newmap_pmt_prob[i]))
        outfile.close()

def get_time_prob_from_r(rootfile, r=100):

    top_map_names = ["topArrayTimeHist1", "topArrayTimeHist2"]

    time_probability = []

    for j in np.arange(np.size(top_map_names)):
        map = top_map_names[j]
        time_hist = rootfile.Get(str(map))
        for i in np.arange(time_hist.GetNbinsY()):
            if i == 0:
                time_probability.append(time_hist.GetBinContent(r,i))
            else:
                time_probability.append(time_hist.GetBinContent(r,i)-time_hist.GetBinContent(r,i-1))


    return time_probability

def compare_maps_pmt_hit_prob_from_bin(oldmap, newmap, r=200, saveoutputs=False):

    oldmap_time_prob = get_time_prob_from_r(oldmap, r)
    newmap_time_prob = get_time_prob_from_r(newmap, r)

    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.plot(oldmap_time_prob, label="oldmap")
    ax.plot(newmap_time_prob, label="newmap")
    ax.set_xlabel("time (ns)")
    ax.set_ylabel("probabilitiy")
    ax.set_title("P(hit PMT in time t nm from r={0}mm)".format(r))
    plt.legend()
    plt.show()

    if saveoutputs:
        outfile = open('/home/ak18773/Documents/LZ/S2LightMaps/newLightMap/pmt_time_prob.txt', "w")
        outfile.write("time \t oldmap \t newmap \n")

        for i in np.arange(np.size(oldmap_time_prob)):
            outfile.write("{0} \t {1} \t {2} \n".format(i, oldmap_time_prob[i], newmap_time_prob[i]))
        outfile.close()
        outfile = open('/home/ak18773/Documents/LZ/S2LightMaps/newLightMap/pmt_time_prob_difference.txt', "w")
        outfile.write("time \t difference \n")

        for i in np.arange(np.size(oldmap_time_prob)):
            outfile.write("{0} \t {1} \n".format(i, oldmap_time_prob[i]-newmap_time_prob[i]))
        outfile.close()

##########
## main ##
##########
if __name__ == "__main__":

    # input file
    oldmap_name = '/home/ak18773/Documents/LZ/S2LightMaps/LightMapRootFiles/Official/S2PhotonMaps-Projected-23Mar18.root'
    newmap_name = '/home/ak18773/Documents/LZ/S2LightMaps/newLightMap/total_lce_3zbins.root'

    # open files
    oldmap_file = ROOT.TFile.Open(oldmap_name, 'read')
    newmap_file = ROOT.TFile.Open(newmap_name, 'read')

    # Compare pmt time prob
    compare_maps_pmt_hit_prob_from_bin(oldmap_file, newmap_file, r=300 ,saveoutputs=False)

    """
    # get probability of hitting PMT given that a PMT will be hit from bin x,y,z
    oldmap_pmt_prob = get_pmt_hit_prob_from_bin(oldmap_file)
    newmap_pmt_prob = get_pmt_hit_prob_from_bin(newmap_file)


    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.plot(oldmap_pmt_prob, label="oldmap")
    ax.plot(newmap_pmt_prob, label="newmap")
    ax.set_xlabel("pmt number")
    ax.set_ylabel("probabilitiy")
    ax.set_title("P(hit pmt given pmt will be hit)")
    plt.legend()
    plt.show()

    outfile = open('/home/ak18773/Documents/LZ/S2LightMaps/newLightMap/pmt_hit_prob.txt', "w")
    outfile.write("pmt \t oldmap \t newmap \n")

    for i in np.arange(np.size(oldmap_pmt_prob)):
        outfile.write("{0} \t {1} \t {2} \n".format(i, oldmap_pmt_prob[i], newmap_pmt_prob[i]))
    outfile.close()
    outfile = open('/home/ak18773/Documents/LZ/S2LightMaps/newLightMap/pmt_hit_prob_difference.txt', "w")
    outfile.write("pmt \t difference \n")

    for i in np.arange(np.size(oldmap_pmt_prob)):
        outfile.write("{0} \t {1} \n".format(i, oldmap_pmt_prob[i]-newmap_pmt_prob[i]))
    outfile.close()

    """
    oldmap_file.Close()
    newmap_file.Close()