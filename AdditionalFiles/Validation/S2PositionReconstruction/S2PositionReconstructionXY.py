"""
Filename:
S2PositionReconstructionXY.py

Description:
Get the difference (R) between reconstruction S2 pulse and truth (mc)

"""

# Imports
import matplotlib.pyplot as plt
import uproot

def readFile(filename, branch, xbranch, ybranch): # not branch... change to better word

    thefile = uproot.open(filename)

    rqs = thefile.get(branch)

    startpos_x = rqs.array(xbranch)
    startpos_y = rqs.array(ybranch)

    return startpos_x, startpos_y


##########
## main ##
##########
if __name__ == "__main__":

    # input file
    ### Should defo move this to cmd args or an input script? maybe yml?
    reconstructedfile_name = '/home/ak18773/Documents/LZ/S2LightMaps/S2LightMap/AdditionalFiles/Validation/S2PositionReconstruction/oldMap_0x_0y_lzap.root'
    truthfile_name = '/home/ak18773/Documents/LZ/S2LightMaps/S2LightMap/AdditionalFiles/Validation/S2PositionReconstruction/oldMap_0x_0y_lzap_mctruth.root'

    truth_startpos_x, truth_startpos_y = readTruth(truthfile_name, 'RQMCTruth', 'mcTruthEvent.parentPositionX_mm', 'mcTruthEvent.parentPositionY_mm')

    tpc_startpos_x, tpc_startpos_y = readFile(reconstructedfile_name, 'Events', 'pulsesTPC.s2Xposition_cm', 'pulsesTPC.s2Yposition_cm')


    ### Now do some plotting
