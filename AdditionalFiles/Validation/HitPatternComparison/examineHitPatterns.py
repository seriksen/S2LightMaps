"""
Filename:
examineHitPatterns.py

Description:
Look at the Hit Patterns

Author:
Sam Eriksen
"""

# Imports
import ROOT
import numpy as np
import matplotlib.pyplot as plt


def get_average_pmt_s2_hits(tree):
    """

    :param tree: ttree
    :return: extracted values - average number of hits and the number of events
    """

    hits_per_event = np.zeros(540)
    n_events = 0
    j = 0
    for event in tree:
        if j < 100:
            n_events+=1
            pmt_hits = event.iPMTHits
            hits_this_event = []
            for i in np.arange(0, 253):
                hits_this_event.append(int(pmt_hits[i]))
            for i in np.arange(0, 300 - 253):
                hits_this_event.append(0)
            for i in np.arange(253, 540 - 300 + 253):
                hits_this_event.append(int(pmt_hits[i]))
            hits_per_event+=np.array(hits_this_event)
        j+=1

    average_hits =  hits_per_event/n_events

    return average_hits, n_events

def show_average_pmt_s2_hits(trees, treenames, title):

    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.set_title(title[0])
    ax.set_title(title[1])
    ax.set_xlabel("pmt number")

    vals = []
    for i in range(np.size(trees)):
        average_hits, n_events = get_average_pmt_s2_hits(trees[i])
        vals.append(average_hits)
        ax.plot(vals[i], label=treenames[i])
    ax.set_ylabel("average pmt hits from {0} events".format(n_events))
    ax.legend()
    plt.show()

    fig1 = plt.figure()
    ax1 = fig1.add_subplot(111)
    ax1.set_title(title[1])
    ax1.set_xlabel("pmt number")

    for i in range(np.size(trees) - 1):
        ratio = vals[0] / vals[i+1] # divide by zero error should probably be fixed at some point
        ax1.plot(ratio, label=treenames[0] + "/" + treenames[i+1])
        thesum = np.sum(vals[0] - vals[i+1])
        print("{0} - {1} sum difference = {2}".format(treenames[0], treenames[i+1], thesum))

    ax1.set_ylabel("average pmt hits (full sim / map) from {0} events".format(n_events))
    ax1.legend()
    plt.show()

    outfile = open('/home/ak18773/Documents/LZ/S2LightMaps/newLightMap/OSComparison.txt', "w")
    outfile.write("Event \t Difference \t label \n")
    for i in range(np.size(ratio)):
        outfile.write("{0} \t {1} \t a \n".format(i, 1.0/ratio[i]))
    outfile.close()

    return vals

def show_event_s2_hitratio(trees, treenames, quanta, title):

    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.set_xlabel("event number")
    ax.set_ylabel("s2 hit ratio (hit/produced)")
    ax.set_title(title)

    for i in range(np.size(trees)):
        vals1 = get_event_quanta(trees[i], quanta[0])
        vals2 = get_event_quanta(trees[i], quanta[1])
        ratio = np.array(vals1)/np.array(vals2)
        ax.plot(ratio, label=treenames[i])

    ax.legend()
    plt.show()

def show_event_quanta(trees, treenames, quanta, title):

    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.set_xlabel("event number")
    ax.set_ylabel(quanta)
    ax.set_title(title)

    for i in range(np.size(trees)):
        vals = get_event_quanta(trees[i], quanta)
        ax.plot(vals, label=treenames[i])


    ax.legend()
    plt.show()


def get_event_quanta(tree, quanta):

    tree_quanta = []
    i = 0
    for event in tree:
        if i < 100:
            eventleaf = event.FindLeaf(quanta).GetValue()
        #quanta = eventleaf.GetValue()
            tree_quanta.append(eventleaf)
            i+=1

    return tree_quanta

def compare_fullsim_os():

    #centosfile = '/home/ak18773/Documents/LZ/S2LightMaps/newLightMap/Validation/centos7/gridElectrons/fullSim1_mctruth.root'
    centosfile = '/home/ak18773/Documents/LZ/S2LightMaps/newLightMap/Validation/centos7/BACC_4.3.3/fullSim1_mctruth.root'
    #slfile = '/home/ak18773/Documents/LZ/S2LightMaps/newLightMap/Validation/sl6/gridElectrons/fullSim1_mctruth.root'
    slfile = '/home/ak18773/Documents/LZ/S2LightMaps/newLightMap/Validation/sl6/BACC_4.3.3/fullSim1_mctruth.root'

    centos = ROOT.TFile.Open(centosfile, 'read')
    sl = ROOT.TFile.Open(slfile, 'read')

    trees = []
    trees.append(centos.Get("MCTruthTree"))
    trees.append(sl.Get("MCTruthTree"))

    treenames = ["centos7", "sl6"]

    show_average_pmt_s2_hits(trees, treenames, "os comparison")

    show_event_s2_hitratio(trees,
                               treenames,
                               ["iS2PhotonHits","iRawS2Photons"],
                               "BACC_S2HitRatio")

    for i in ["iRawS2Photons", "iS2PhotonHits", "iRawS1Photons"]:
        show_event_quanta(trees, treenames, i, "os comparison")



########
# main #
########
if __name__ == "__main__":

    # Get ROOTFile
    filelocation = '/home/ak18773/Documents/LZ/S2LightMaps/newLightMap/Validation'
    os, bacc_release = ['centos7', 'gridE']
    #os = 'sl6'
    #bacc_release = '3.5.0'
    #bacc_release = '4.3.3'

    # To just compare OS's types
    compare_fullsim_os()

    # There are also special cases such as with different projected values etc... and edits to files of BACCARAT
    # But these aren't as useful
    filelocation = '/home/ak18773/Documents/LZ/S2LightMaps/newLightMap/Validation'

    fullsimfile = filelocation + '/' + os
    oldmapfile = filelocation + '/' + os
    newmapfile = filelocation + '/' + os

    if bacc_release == 'gridE':
        print("gridE selected")
        bacc_release = '4.3.3'
        fullsimfile+='/gridElectrons/fullSim1_mctruth.root'
        oldmapfile+='/gridElectrons/oldMap1_mctruth.root'
        newmapfile+='/gridElectrons/newMap1_mctruth.root'
    else:
        fullsimfile+= '/BACC_' + bacc_release + '/fullSim1_mctruth.root'
        oldmapfile+= '/BACC_' + bacc_release + '/oldMap1_mctruth.root'
        newmapfile+= '/BACC_' + bacc_release + '/newMap1_mctruth.root'


    # Open files and extract tree
    mctruthtrees = []
    treenames = []

    try:
        fullsim = ROOT.TFile.Open(fullsimfile, 'read')
        mctruthtrees.append(fullsim.Get("MCTruthTree"))
        treenames.append("full sim")
    except:
        print("no full sim")
    try:
        oldmap  = ROOT.TFile.Open(oldmapfile, 'read')
        mctruthtrees.append(oldmap.Get("MCTruthTree"))
        treenames.append("old map")
    except:
        print("no old map {0}".format(oldmapfile))
    try:
        newmap  = ROOT.TFile.Open(newmapfile, 'read')
        mctruthtrees.append(newmap.Get("MCTruthTree"))
        treenames.append("new map")
    except:
        print("no new map")

    # Determine version of BACCARAT used (difference if pre 3.5.0)

    if float(bacc_release[:-2]) >= 3.5: # contains S2Raw and S2Hits

        quanta = ["iRawS2Photons",
                  "iS2PhotonHits",
                  "iRawS1Photons"]

        for i in quanta:
            print(i)

            show_event_quanta(mctruthtrees,
                              treenames,
                              i,
                              "BACC_4.7.5_centos7_" + i)


        # Now get the hit ratio
        show_event_s2_hitratio(mctruthtrees,
                               treenames,
                               ["iS2PhotonHits","iRawS2Photons"],
                               "BACC_4.7.5_centos7_S2HitRatio")

        vals = show_average_pmt_s2_hits(mctruthtrees,
                                        treenames,
                                       ["BACC_4.7.5_centos7_pmtHitAverage", "BACC_4.7.5_centos7_pmtHitAverageRatio"])

        fig = plt.figure()
        ax = fig.add_subplot(111)
        mapdifference = np.array(vals[1]) - np.array(vals[2])
        mappercentdiff = (np.array(vals[1]) - np.array(vals[2])) * 100 /((np.array(vals[1]) + np.array(vals[2]))/2)
        ax.plot(mapdifference)
        ax.set_ylabel("difference")
        ax.set_xlabel("pmt number")
        ax.set_title("Difference (file1 - file2)")
        plt.show()

        fig = plt.figure()
        ax = fig.add_subplot(111)
        ax.plot(mappercentdiff)
        ax.set_ylabel("percent difference")
        ax.set_xlabel("pmt number")
        ax.set_title("Difference (file1 - file2)/((file1 + file2)/2) * 100")
        plt.show()


    else: # contains S2Quanta

        show_event_quanta(mctruthtrees,
                          treenames,
                          "iS2Quanta",
                          "BACC_" + bacc_release + "_" + os + "_" + "iS2PhotonHits")

        show_event_quanta(mctruthtrees,
                          treenames,
                          "iS1Quanta",
                          "BACC_" + bacc_release + "_" + os + "_" + "iS1PhotonHits")

        show_average_pmt_s2_hits(mctruthtrees,
                                 treenames,
                                 ["BACC_" + bacc_release + "_" + os + "_pmtHitAverage", "BACC_" + bacc_release + "_" + os + "_pmtHitAverageRatio"])

    # Close Files
    try:
        fullsim.Close()
        oldmap.Close()
        newmap.Close()
    except:
        print("some files failed to close... maybe they were never open...")

