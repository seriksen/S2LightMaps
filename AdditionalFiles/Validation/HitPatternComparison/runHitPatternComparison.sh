#!/bin/bash
#
# NERSC CORI
#SOURCE_DIR=/global/projecta/projectdirs/lz/data/bristol/s2maps/S2LightMap/hitPatternTest
#BACC_DIR=/global/projecta/projectdirs/lz/data/bristol/s2maps/S2LightMap/BACCARAT
#LZ_BUILD_CONFIG=x86_64-centos7-gcc7-opt
#
# SOOLIN
SOURCE_DIR=/users/ak18773/AdditionalFiles/Validation/HitPatternComparison
BACC_DIR=/users/ak18773/BACCARAT
LZ_BUILD_CONFIG=x86_64-slc6-gcc48-opt
#
#
export OUTPUT_DIR=${SOURCE_DIR}
NELECTRONS=20
MACRONAME="s2electronEmission"
SEED=1
#
#
########################
## Setup for BACCARAT ##
########################
BACC_BUILD=${BACC_DIR}/build/${LZ_BUILD_CONFIG}
BACC_EXE=${BACC_BUILD}/bin
source ${BACC_DIR}/setup.sh
#
#
#simType="fullSim"
#simBOOL="false"
#simType="oldMap"
simBOOL="true"
simType="newMap"
FILENAME=${simType}
####################
## BACCARAT macro ##
####################
export MACRO_COPY_PATH=${OUTPUT_DIR}/${MACRONAME}_${simType}.mac
cp ${MACRONAME}.mac ${MACRO_COPY_PATH}
echo "/control/getEnv" "${OUTPUT_DIR}" >> ${MACRO_COPY_PATH}
echo "/Bacc/io/outputDir" "${OUTPUT_DIR}" >> ${MACRO_COPY_PATH}
echo "/Bacc/io/outputName" "${FILENAME}" >> ${MACRO_COPY_PATH}
echo "/Bacc/detector/useMapOptical" "${simBOOL}" >> ${MACRO_COPY_PATH}
echo "/Bacc/randomSeed" "${SEED}" >> ${MACRO_COPY_PATH}
echo "/Bacc/beamOn" "${NELECTRONS}" >> ${MACRO_COPY_PATH}
echo "exit" >> ${MACRO_COPY_PATH}
#
#
####################
## Run Simulation ##
####################
echo "Starting BACCARAT with ${simType}"
${BACC_EXE}/BACCARATExecutable ${MACRO_COPY_PATH}
#
#
##############################
## Convert bin to root file ##
##############################
echo "Converting ${FILENAME}.bin to root"
${BACC_EXE}/BaccRootConverter ${OUTPUT_DIR}/${FILENAME}${SEED}.bin
# Remove bin file
rm ${OUTPUT_DIR}/${FILENAME}${SEED}.bin
#
#
#########################
## Produce S2 Hit Maps ##
#########################
echo "Running MCTruth on ${FILENAME}.root"
${BACC_EXE}/BaccMCTruth ${OUTPUT_DIR}/${FILENAME}${SEED}.root
