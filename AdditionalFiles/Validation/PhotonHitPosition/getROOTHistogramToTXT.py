"""
Filename:
getROOTHistogramToTXT.py

Description:
Convert a root file to a txt file for latex plotting

Output:
- txt file

"""

# Imports
import matplotlib.pyplot as plt
import ROOT
import numpy as np
from AdditionalFiles.Validation.PMT_Locations import topArrayRs, bottomArrayRs

def fillTxt(projection):
    nbins = projection.GetNbinsX()
    minbin = projection.GetXaxis().GetBinLowEdge(1)
    maxbin = projection.GetXaxis().GetBinUpEdge(nbins)
    binwidth = projection.GetBinWidth(0)
    bins = np.arange(minbin, maxbin + binwidth, binwidth)
    print(bins)
    print(minbin)
    print(maxbin)
    print(nbins)
    vals = []
    for i in range(nbins):
        vals.append(projection.GetBinContent(i))
    return bins, vals

def getValsFromBins(branch):
    nbins = branch.GetNbinsX()
    values = []
    print(nbins)
    for i in range(nbins+1):
        values.append(branch.GetBinContent(i))
    return values

def get_pmt_projection_binvals(pmt, xyz):

    projection = pmt.Projection3D(xyz)

    if xyz == "x":
        nbins = projection.GetNbinsX()
        minbin = projection.GetXaxis().GetBinLowEdge(1)
        maxbin = projection.GetXaxis().GetBinUpEdge(nbins)
    elif xyz == "y":
        nbins = projection.GetNbinsY()
        minbin = projection.GetYaxis().GetBinLowEdge(1)
        maxbin = projection.GetYaxis().GetBinUpEdge(nbins)
    elif xyz == "z":
        nbins = projection.GetNbinsZ()
        minbin = projection.GetZaxis().GetBinLowEdge(1)
        maxbin = projection.GetZaxis().GetBinUpEdge(nbins)
    else:
        print("well you've done something wrong, we shouldn't be here")
        print("for your info, projection = {0} but it should equal x, y or z".format(xyz))
        return -1, -1

    binwidth = projection.GetBinWidth(0)
    bins = np.arange(minbin, maxbin + binwidth, binwidth)
    print(bins)
    print(minbin)
    print(maxbin)
    print(nbins)
    vals = []
    for i in range(nbins):
        vals.append(projection.GetBinContent(i))
    return bins, vals

def get_pmt_names(rootfile):

    pmts = []
    for i in np.arange(0,253):
        pmts.append("Top_PMT_Photocathode_{0}_I".format(i))
    for i in np.arange(300,541):
        pmts.append("Bottom_PMT_Photocathode_{0}_I".format(i))
    return pmts

##########
## main ##
##########
if __name__ == "__main__":

    # input file
    rootFileName = '/home/ak18773/Documents/LZ/S2LightMaps/newLightMap/total_hits_3zbins.root'
    # open Lzap
    rootFile = ROOT.TFile.Open(rootFileName, 'read')

    # Get TTrees of interest
   # pmt_names = get_pmt_names(rootFile)
    whichPMT = "Bottom_PMT_Photocathode_300_hits"
    whichaxis = "x"
    PMT = rootFile.Get(whichPMT)
    projection = PMT.Project3D(whichaxis)

    bins, vals = fillTxt(projection)

    outfile = open('/home/ak18773/Documents/LZ/S2LightMaps/newLightMap/{0}_{1}.txt'.format(whichPMT, whichaxis), "w")
    outfile.write("Lower \t Upper \t Count \n")
    for i in range(np.size(vals)):
        outfile.write("{0} \t {1} \t {2} \n".format(bins[i],bins[i+1],vals[i]))

    outfile.close()

    plt.plot(bins[:-1], vals)
    plt.xlabel("position (mm)")
    plt.ylabel("no. hits")
    plt.title("{0} {1} axis".format(whichPMT, whichaxis))
    plt.show()
    print(vals)
    rootFile.Close()
    #plt.savefig('.png')

    ###################
    ## Look at Means ##
    ###################
    rootFileName = '/home/ak18773/Documents/LZ/S2LightMaps/newLightMap/S2MapComparison.root'

    rootFile = ROOT.TFile.Open(rootFileName, 'read')


    xMeanVals_top_file1 = getValsFromBins(rootFile.Get("topPMT_xMean_file1"))
    yMeanVals_top_file1 = getValsFromBins(rootFile.Get("topPMT_yMean_file1"))
    zMeanVals_top_file1 = getValsFromBins(rootFile.Get("topPMT_zMean_file1"))
    xMeanVals_top_file2 = getValsFromBins(rootFile.Get("topPMT_xMean_file2"))
    yMeanVals_top_file2 = getValsFromBins(rootFile.Get("topPMT_yMean_file2"))
    zMeanVals_top_file2 = getValsFromBins(rootFile.Get("topPMT_zMean_file2"))
    xMeanVals_top_difference = getValsFromBins(rootFile.Get("topPMT_xMean_difference"))
    yMeanVals_top_difference = getValsFromBins(rootFile.Get("topPMT_yMean_difference"))
    zMeanVals_top_difference = getValsFromBins(rootFile.Get("topPMT_zMean_difference"))

    xMeanVals_bottom_file1 = getValsFromBins(rootFile.Get("bottomPMT_xMean_file1"))
    yMeanVals_bottom_file1 = getValsFromBins(rootFile.Get("bottomPMT_yMean_file1"))
    zMeanVals_bottom_file1 = getValsFromBins(rootFile.Get("bottomPMT_zMean_file1"))
    xMeanVals_bottom_file2 = getValsFromBins(rootFile.Get("bottomPMT_xMean_file2"))
    yMeanVals_bottom_file2 = getValsFromBins(rootFile.Get("bottomPMT_yMean_file2"))
    zMeanVals_bottom_file2 = getValsFromBins(rootFile.Get("bottomPMT_zMean_file2"))
    xMeanVals_bottom_difference = getValsFromBins(rootFile.Get("bottomPMT_xMean_difference"))
    yMeanVals_bottom_difference = getValsFromBins(rootFile.Get("bottomPMT_yMean_difference"))
    zMeanVals_bottom_difference = getValsFromBins(rootFile.Get("bottomPMT_zMean_difference"))

    outfile = open('/home/ak18773/Documents/LZ/S2LightMaps/newLightMap/topArrayMeansVsDistance.txt', "w")
    outfile.write("Distance \t OldMapX \t OldMapY \t OldMapZ \t NewMapX \t NewMapY \t NewMapZ \t DifferenceX \t DifferenceY \t DifferenceZ \t label \n")

    for i in range(np.size(xMeanVals_bottom_file1)):
        outfile.write("{0} \t {1} \t {2} \t {3} \t {4} \t {5} \t {6} \t {7} \t {8} \t {9} \t a \n".format(topArrayRs[i], xMeanVals_top_file1[i], yMeanVals_top_file1[i], zMeanVals_top_file1[i], xMeanVals_top_file2[i], yMeanVals_top_file2[i], zMeanVals_top_file2[i], xMeanVals_top_difference[i], yMeanVals_top_difference[i], zMeanVals_top_difference[i]))
    outfile.close()

    outfile = open('/home/ak18773/Documents/LZ/S2LightMaps/newLightMap/bottomArrayMeansVsDistance.txt', "w")
    outfile.write("Distance \t OldMapX \t OldMapY \t OldMapZ \t NewMapX \t NewMapY \t NewMapZ \t DifferenceX \t DifferenceY \t DifferenceZ \t label \n")

    for i in range(np.size(xMeanVals_bottom_file1)):
        outfile.write("{0} \t {1} \t {2} \t {3} \t {4} \t {5} \t {6} \t {7} \t {8} \t {9} \t a \n".format(bottomArrayRs[i], xMeanVals_bottom_file1[i], yMeanVals_bottom_file1[i], zMeanVals_bottom_file1[i], xMeanVals_bottom_file2[i], yMeanVals_bottom_file2[i], zMeanVals_bottom_file2[i], xMeanVals_bottom_difference[i], yMeanVals_bottom_difference[i], zMeanVals_bottom_difference[i]))
    outfile.close()

    topOrBot = 'Top' #Bottom'
    xyz = 'X'
    plt.scatter(bottomArrayRs, zMeanVals_bottom_file1, label='old map')
    plt.title('{0} PMT Array {1}-axis file1'.format(topOrBot, xyz))
    plt.xlabel('distance from centre (mm)')
    plt.ylabel('mean (mm)')
    plt.savefig('/home/ak18773/Documents/LZ/S2LightMaps/newLightMap/{0}Array{1}axisOldMap.png'.format(topOrBot, xyz))
    plt.show()
    #plt.close()
    plt.scatter(bottomArrayRs, zMeanVals_bottom_file2, label='new map')
    plt.title('{0} PMT Array {1}-axis file1'.format(topOrBot, xyz))
    plt.xlabel('distance from centre (mm)')
    plt.ylabel('mean (mm)')
    plt.savefig('/home/ak18773/Documents/LZ/S2LightMaps/newLightMap/{0}Array{1}axisNewMap.png'.format(topOrBot, xyz))
    plt.show()
    #plt.close()
    plt.scatter(bottomArrayRs, zMeanVals_bottom_difference, label='difference')
    plt.title('{0} PMT Array {1}-axis difference'.format(topOrBot, xyz))
    plt.xlabel('distance from centre (mm)')
    plt.ylabel('mean (mm)')
    plt.savefig('/home/ak18773/Documents/LZ/S2LightMaps/newLightMap/{0}Array{1}axisDifference.png'.format(topOrBot,xyz))
    plt.show()
    #plt.close()

