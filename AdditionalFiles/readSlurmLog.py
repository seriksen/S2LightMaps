"""
Extract metrics from output of execute_with_metrics BACCARAT function
"""


def getBACCARATtime(filename):
        thefile = open(filename, "r")

        for line in thefile:
                if line.startswith("Run 0 time"):
                        lineArray = line.split()
                        return int(lineArray[5])


if __name__ == "__main__":
        startFile = 1
        endFile = 2000
        filebase = "slurm_"
        fileend = ".out"
        times = []

        for i in range(startFile, endFile+1):

                filename = filebase + str(i) + fileend
                times.append(getBACCARATtime(filename))

        print "mean time: ", sum(times)/len(times)
        print "max time: ", max(times)
        print "min time: ", min(times)
        print "number of files: ",  len(times)
