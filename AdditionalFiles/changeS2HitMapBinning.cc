/*
 * Filename:
 * changeS2HitMapBinning.cc
 *
 * Description:
 * Re-bins the histogram data from x many z bins to 3 (to allow comparison with older light maps)
 * grams output from produceS2HitMaps
 * The output of this is the S2LightMap used in MCTruth
 *
 * Compile Instructions:
 * source BACC_DIR/setup.sh
 * g++ changeS2HitMapBinning.cc -o changeS2HitmapBinning `root-config --cflags --glibs`
 *
 * Input:
 * - .root file (output from produceS2HitMaps which contains a number of histograms)
 *    See code for what histograms are required along with the required names.
 *
 * Output:
 * - .root file containing histograms of PMT hits
 *
 */

// C++/C includes
#include <stdio.h>
#include <iostream>

// ROOT includes
#include <TH3F.h>
#include <TH3I.h>
#include <TH2F.h>
#include <TH2I.h>
#include <TAxis.h>
#include <TFile.h>
#include <TTree.h>

int main(int argc, char* argv[]){

	std::string outfileName = "";

	/***************
     * Check input *
     ***************/
	try{
		if(argc<2) throw "No filename specified";
		std::string fileName = std::string(argv[1]);
		outfileName = fileName;
		fileName = fileName.substr(fileName.length()-5,5);
		if(fileName!=".root") throw "Not a ROOT file";
		outfileName = outfileName.substr(0,outfileName.length()-10);
		outfileName += "_hits_3zbins.root";
		TFile* infile = new TFile(argv[1]);
		if(infile->IsZombie()) throw "Error opening file";
		if(!infile->GetListOfKeys()->Contains("startPos"))
			throw "File does not contain the start position histogram";
	}
	catch(const char* msg){
		std::cerr << msg << " - changeS2HitMapBinning will exit" << std::endl;
		return 1;
	}

	/**************
	 * Read input *
	 **************/
	TFile* infile = new TFile(argv[1],"read");
	size_t noOfMaps = infile->GetListOfKeys()->GetSize();

	// Setup Outfile
	TFile* outfile = new TFile(outfileName.c_str(),"recreate");

	// Copy start position maps
	TH3I* startPos = (TH3I*)infile->Get("startPos");
	TH3I* startPosHist = new TH3I(*((TH3I*)startPos));
	TH3I* primaryParticleStartPos = (TH3I*)infile->Get("primaryParticleStartPos");
	TH3I* primaryParticleStartPosHist = new TH3I(*((TH3I*)primaryParticleStartPos));

	std::cout << "Copied startPos and primaryParticleStartPos" << std::endl;

	/************
     * Hit Maps *
     ************/
	// Copy all PMT hits maps
	std::vector<TH3I*> hitHists;
	int noOfHitMaps{0};

	for(size_t i = 0; i<noOfMaps; ++i) {
		if (i == 0) {
			TH3I* thisMap = (TH3I*) infile->Get("allPMTHits");
			TH3I* thisMapCopy =  new TH3I(*((TH3I*)thisMap));
			hitHists.push_back(thisMapCopy);

			++noOfHitMaps;
		}
		std::string mapName = std::string(infile->GetListOfKeys()->At(i)->GetName());
		if (mapName == "startPos" || mapName == "allPMTHits")
			continue;

		else if (mapName.find("PMT") != std::string::npos) {
			TH3I* thisMap = (TH3I*) infile->Get(mapName.c_str());
			TH3I* thisMapCopy =  new TH3I(*((TH3I*)thisMap));
			hitHists.push_back(thisMapCopy);
			++noOfHitMaps;
		}
	}

	std::cout << "Copied all PMT hitMaps" << std::endl;

	// Rebin all entries histograms in hitHists and start pos histograms
	for (size_t i = 0; i<noOfHitMaps; ++i) {
		// Merge 15 bins into 3 bins
		hitHists[i]->RebinZ(5);
	}
	startPosHist->RebinZ(5);
	primaryParticleStartPosHist->RebinZ(5);

	std::cout << "Rebinned HitMaps" << std::endl;

	/*************
     * Time Maps *
     *************/
	// These do not need to be changed as they do not depend on the binning
	// Can just copy from the input file
	TH2I* topCountsHist1 = (TH2I*)infile->Get("topArrayTimeHist1");
	TH2I* topCountsHist2 = (TH2I*)infile->Get("topArrayTimeHist2");
	TH2I* bottomCountsHist1 = (TH2I*)infile->Get("bottomArrayTimeHist1");
	TH2I* bottomCountsHist2 = (TH2I*)infile->Get("bottomArrayTimeHist2");
	TH2F* topHist1 = new TH2F(*((TH2F*)topCountsHist1));
	TH2F* topHist2 = new TH2F(*((TH2F*)topCountsHist2));
	TH2F* bottomHist1 = new TH2F(*((TH2F*)bottomCountsHist1));
	TH2F* bottomHist2 = new TH2F(*((TH2F*)bottomCountsHist2));


	/*****************************
	 * Write outputs and tidy up *
	 *****************************/
	//Write out the maps to the outfile
	primaryParticleStartPosHist->Write(primaryParticleStartPosHist->GetName());
	startPosHist->Write(startPosHist->GetName());

	for(int i = 0; i<noOfHitMaps; ++i){
		hitHists[i]->Write(hitHists[i]->GetName());
		delete hitHists[i];
	}

	topHist1->Write(topHist1->GetName());
	topHist2->Write(topHist2->GetName());
	bottomHist1->Write(bottomHist1->GetName());
	bottomHist2->Write(bottomHist2->GetName());
	delete topHist1;
	delete topHist2;
	delete bottomHist1;
	delete bottomHist2;

	infile->Close();
	delete infile;
	outfile->Close();
	delete outfile;

}
