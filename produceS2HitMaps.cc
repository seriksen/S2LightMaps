/*
 * Filename:
 * produceS2HitMaps.cc
 *
 * Description: 
 * Generate histograms for where photons go after BACCARAT simulation
 *
 * Compile Instructions:
 * source BACC_DIR/setup.sh
 * g++ produceS2HitMaps.cc ${BACC_BUILD}/tools/BaccRootConverterEvent_dict.cxx -o produceS2HitMaps -I$BACC_DIR -I$BACC_DIR/tools -I$BACC_DIR/build_${LZ_BUILD_CONFIG}/tools -I$ROOT_DIR `root-config --cflags --glibs`
 *
 * Input:
 * - .root file (converted output from BACCARAT) which contains a HeaderTree and a DataTree
 *
 * Output:
 * - .root file containing histograms of PMT hits
 *
 * Unresolved Issues:
 * - TCling not recognised when run
 *
 */


// C++/C includes
#include <stdio.h>
#include <iostream>

// ROOT includes
#include <TH3I.h>
#include <TH2I.h>
#include <TAxis.h>
#include <TFile.h>
#include <TTree.h>
#include <TH3F.h>

// BACCARAT includes
#include "LZ/include/LZDetectorPMTBankCoordinates.hh"
#include "tools/BaccRootConverterEvent.hh"

using namespace std;

int main(int argc, char* argv[]){

	string outfileName = "";

	/************************
	 * Check the input file *
	 ************************/
	try{
		if(argc<2) throw "No filename specified";
		string fileName = string(argv[1]);
		outfileName = fileName;
		fileName = fileName.substr(fileName.length()-5,5);
		if(fileName!=".root") throw "Not a ROOT file";
		outfileName = outfileName.substr(0,outfileName.length()-5);
		outfileName += "_hits.root";
		TFile* infile = new TFile(argv[1]);
		if(infile->IsZombie()) throw "Error opening file";
		if(!infile->GetListOfKeys()->Contains("HeaderTree"))
			throw "File does not contain the HeaderTree";
		if(!infile->GetListOfKeys()->Contains("DataTree"))
			throw "File does not contain the DataTree";
	}
	catch(const char* msg){
		cerr << msg << " - produceS2HitMaps will exit" << endl;
		return 1;
	}

	// Read input file
	TFile* infile = new TFile(argv[1],"read");
	TTree* headerTree = (TTree*)infile->Get("HeaderTree");

	/*********************
	 * HitMap histograms *
	 *********************/
	// Set up the histogram dimensions of your hit maps
	// Here, we set up for the phase boundary-anode gas gap
	// For the S2 maps in the LZ TPC

	// Create outfile
	TFile* outfile = new TFile(outfileName.c_str(),"recreate");

	//check 0
	std::cout << "Check 0" << std::endl;

	vector<string>* components = 0;
	headerTree->SetBranchAddress("componentLookupTable",&components);
	headerTree->GetEntry(0);

	// Define Hit Map bins
	double xMax{750};
	double xMin{-750}; // To stretch into the weir gap
	double yMax{750};
	double yMin{-750}; // To stretch into the weir gap
	double zMax{1472.5};
	double zMin{1457.5}; // To cover the gas gap from 1461 to 1469 mm

	double xBinWidth{5};
	double yBinWidth{5};
	double zBinWidth{1}; //5};

	int nBinsX = (xMax-xMin)/xBinWidth;
	int nBinsY = (yMax-yMin)/yBinWidth;
	int nBinsZ = (zMax-zMin)/zBinWidth;

	TH3I* primaryParticleStartHist = new TH3I("primaryParticleStartPos","primaryParticleStartPos",
											  nBinsX,xMin,xMax,
											  nBinsY,yMin,yMax,
											  nBinsZ,zMin,zMax);
	primaryParticleStartHist->GetXaxis()->SetTitle("x (mm)");
	primaryParticleStartHist->GetYaxis()->SetTitle("y (mm)");
	primaryParticleStartHist->GetZaxis()->SetTitle("z (mm)");

	TH3F* startHist = new TH3F("startPos","startPos",
							   nBinsX,xMin,xMax,
							   nBinsY,yMin,yMax,
							   nBinsZ,zMin,zMax);
	startHist->GetXaxis()->SetTitle("x (mm)");
	startHist->GetYaxis()->SetTitle("y (mm)");
	startHist->GetZaxis()->SetTitle("z (mm)");

	vector<TH3I*> hitHists;
	hitHists.push_back(new TH3I("allPMTHits","allPMTHits",
								nBinsX,xMin,xMax,
								nBinsY,yMin,yMax,
								nBinsZ,zMin,zMax));

	/******************************
	 * Set up Time Map Histograms *
	 ******************************/
	// These are segmented into two maps for both top and bottom PMT arrays.
	// There seems to be a clear divide here between photons that presumably hit the PMT directly versus those that bounce.
	// The histograms contain photon counts for nanoseconds taken to hit.
	const int topNBinsX{1500};
	const double topXMin{0};
	const double topXMax{1500};
	const int topNBinsY1{100};
	const int topNBinsY2{540};
	const double topYMin1{0};
	const double topYMax1{10};
	const double topYMax2{550};

	const int botNBinsX{570};
	const double botXMin{1606};
	const double botXMax{2176};
	const int botNBinsY1{20};
	const int botNBinsY2{648};
	const double botYMin1{0};
	const double botYMax1{2};
	const double botYMax2{650};

	TH2I* topArrayTimeHist1 = new TH2I(
			"topArrayTimeHist1","topArrayTimeHist1",
			topNBinsX, topXMin, topXMax,
			topNBinsY1, topYMin1, topYMax1 );
	topArrayTimeHist1->GetXaxis()->SetTitle("r (mm)");
	topArrayTimeHist1->GetYaxis()->SetTitle("time (ns)");
	TH2I* topArrayTimeHist2 = new TH2I(
			"topArrayTimeHist2","topArrayTimeHist2",
			topNBinsX, topXMin, topXMax,
			topNBinsY2, topYMax1, topYMax2);
	topArrayTimeHist2->GetXaxis()->SetTitle("r (mm)");
	topArrayTimeHist2->GetYaxis()->SetTitle("time (ns)");
	TH2I* bottomArrayTimeHist1 = new TH2I(
			"bottomArrayTimeHist1","bottomArrayTimeHist1",
			botNBinsX, botXMin, botXMax,
			botNBinsY1, botYMin1, botYMax1);
	bottomArrayTimeHist1->GetXaxis()->SetTitle("r (mm)");
	bottomArrayTimeHist1->GetYaxis()->SetTitle("time (ns)");
	TH2I* bottomArrayTimeHist2 = new TH2I(
			"bottomArrayTimeHist2","bottomArrayTimeHist2",
			botNBinsX, botXMin, botXMax,
			botNBinsY2, botYMax1, botYMax2);
	bottomArrayTimeHist2->GetXaxis()->SetTitle("r (mm)");
	bottomArrayTimeHist2->GetYaxis()->SetTitle("time (ns)");

	/*********************************
	 * Set up PMT Hit Map Histograms *
	 *********************************/
	//Identify which G4 volumes correspond to PMTs,
	//and set up histograms for them
	vector<int> pmtIndex;
	vector<int> histIndex;

	int noOfHitMaps{1}; // start at 1 to include allPMTHits

	for(size_t i = 0; i<components->size(); ++i){
		TString component = components->at(i);
		if(component.Contains("PMT_Photocathode") && !component.Contains("Skin") && (component.Contains("Top") || component.Contains("Bottom"))){

			// Get pmt number
			TString pmtString = component(component.Length()-3,3);
			int pmtID = pmtString.Atoi();
			pmtIndex.push_back(pmtID);

			TString hitMapName = component;
			hitMapName.Append("_hits");

			hitHists.push_back(new TH3I(
					hitMapName,hitMapName,
					nBinsX,xMin,xMax,
					nBinsY,yMin,yMax,
					nBinsZ,zMin,zMax));

			histIndex.push_back(noOfHitMaps);
			++noOfHitMaps;
		}
		else{
			pmtIndex.push_back(-1);
			histIndex.push_back(-1);
		}
	}

	// Loop through hitHists giving them xyz labels
	for (size_t i = 0; i<hitHists.size(); ++i){

		hitHists[i]->GetXaxis()->SetTitle("x (mm)");
		hitHists[i]->GetYaxis()->SetTitle("y (mm)");
		hitHists[i]->GetZaxis()->SetTitle("z (mm)");
	}

	// Variable for filling histograms
	double x,y,z,pmtX,pmtY,pmtZ,r,t;
	x = y = z = pmtX = pmtY = pmtZ = r = t = 0;
	int volumeID{0};
	int pmt{0};
	int xBin, yBin, zBin;
	xBin = yBin = zBin;
	double topArrayZ{1539.5}; // Z position of PMT arrays
	double bottomArrayZ{-148};
	int updateFrequency{10000}; // for outputting to terminal

	/************************
	 * Loop over data entry *
	 ************************/
	// Get dataTree
	TTree* dataTree = (TTree*)infile->Get("DataTree");
	BaccRootConverterEvent* event{0};
	dataTree->SetBranchAddress("Event",&event);

	// Loop over each data entry in the input root file
	// For each data entry, populate the relevant histogram
	// Fill the start position histogram by looking at the creation step,
	// and the hit histograms by looking at the volume of the last step

	for(int i = 0; i<dataTree->GetEntries(); ++i){
		if(i%updateFrequency==0) cout << "Entry " << i << " of " <<  dataTree->GetEntries() << endl;

		dataTree->GetEntry(i);

		// Loop over each event track
		for(int j = 0; j<event->tracks.size(); ++j){

			// Extract photon start position
			x = event->tracks[j].steps[0].dPosition_mm[0];
			y = event->tracks[j].steps[0].dPosition_mm[1];
			z = event->tracks[j].steps[0].dPosition_mm[2];

			// Extract photon lifetime
			t = event->tracks[j].steps.back().dTime_ns - event->tracks[j].steps[0].dTime_ns;

			// Get volume ID of where the photon ended
			volumeID = event->tracks[j].steps.back().iVolumeID-1;

			// The start positions are the step[0] so can fill the start position histogram
			startHist->Fill(x,y,z);

			// Just primary particle start positions
			if (j == 0)
			{
				primaryParticleStartHist->Fill(x,y,z);
			}

			// If the photon hits the volume
			if(histIndex.at(volumeID)>-1){

				// Populate the hit histogram
				hitHists[0]->Fill(x,y,z);

				// Populate the histogram associated with the PMT which was hit
				hitHists[histIndex.at(volumeID)]->Fill(x,y,z);

				// Populate the time maps //

				// Get pmt number
				pmt = pmtIndex.at(volumeID);

				// pmt numbering: There are 252 PMTs in the top array and
				// 540-300 = 240 PMTs in the bottom array
				// The numbering follows:
				// 0-251 in the Top Array
				// 300-540 in the Bottom Array

				// If photon hits a PMT in the top array
				if(pmt!=-1 && pmt<253){

					// get pmt xyz
					pmtX = TopPMTArrayXY[pmt][0]; // value in mm
					pmtY = TopPMTArrayXY[pmt][1]; // value in mm
					pmtZ = topArrayZ;

					// get photon start position distance from pmt
					r = pow(pow((x-pmtX),2)+pow((y-pmtY),2)+pow((z-pmtZ),2),0.5);

					// if the time taken to hit is greater than topYMax1 = 10ns
					if(t>=10) topArrayTimeHist2->Fill(r,t);
					else if(t<10) topArrayTimeHist1->Fill(r,t);

				}

				// If photon hits a PMT in the bottom array
				else if(pmt!=-1){

					// Due to pmt numbering -300
					pmt -= 300;

					// Get pmt xyz
					pmtX = BottomPMTArrayXY[pmt][0];
					pmtY = BottomPMTArrayXY[pmt][1];
					pmtZ = bottomArrayZ;


					// Get photon start position from pmt
					r = pow(pow((x-pmtX),2)+pow((y-pmtY),2)+pow((z-pmtZ),2),0.5);

					// If the time taken to hit the pmt is greater than botYMax1 = 2ns
					if(t>=2) bottomArrayTimeHist2->Fill(r,t);
					else if(t<2) bottomArrayTimeHist1->Fill(r,t);
				}

			} // volume check
		} // events
	} // entries


	/*****************************
	 * Tidy up and write outputs *
	 *****************************/
	infile->Close();
	delete infile;

	//Write out the maps to the outfile
	primaryParticleStartHist->Write(primaryParticleStartHist->GetName());
	startHist->Write(startHist->GetName());
	for(int i = 0; i<noOfHitMaps; ++i){
		hitHists[i]->Write(hitHists[i]->GetName());
		delete hitHists[i];
	}
	topArrayTimeHist1->Write(topArrayTimeHist1->GetName());
	topArrayTimeHist2->Write(topArrayTimeHist2->GetName());
	bottomArrayTimeHist1->Write(bottomArrayTimeHist1->GetName());
	bottomArrayTimeHist2->Write(bottomArrayTimeHist2->GetName());
	delete topArrayTimeHist1;
	delete topArrayTimeHist2;
	delete bottomArrayTimeHist1;
	delete bottomArrayTimeHist2;

	outfile->Close();
	delete outfile;

	return 0;
}
