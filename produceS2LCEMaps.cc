/*
 * Filename:
 * produceS2LCEMaps.cc
 *
 * Description:
 * Generate LCE maps from the histograms output from produceS2HitMaps
 * The output of this is the S2LightMap used in MCTruth
 *
 * Compile Instructions:
 * source BACC_DIR/setup.sh
 * g++ produceS2LCEMaps.cc -o produceS2LCEMaps `root-config --cflags --glibs`
 *
 * Input:
 * - .root file (output from produceS2HitMaps which contains a number of histograms)
 *    See code for what histograms are required along with the required names.
 *
 * Output:
 * - .root file containing histograms
 * Histogram map list:
 * - LCE map for each PMT
 * - Cumulative probability map for photon time
 * - Probability map for photon time
 *
 */


// C++/C includes
#include <stdio.h>
#include <iostream>

// ROOT includes
#include <TH3F.h>
#include <TH3I.h>
#include <TH2F.h>
#include <TH2I.h>
#include <TAxis.h>
#include <TFile.h>
#include <TTree.h>

using namespace std;


void fillProbabilityTimeHistograms (TH2F* &cumulativeTimeHist1,
                                    TH2F* &cumulativeTimeHist2,
                                    TH2F* &probTimeHist1,
                                    TH2F* &probTimeHist2,
                                    TH2I* &CountsHist1,
                                    TH2I* &CountsHist2);

void fillLCEHistograms (TH3I* &startPos,
                        vector<TH3I*> &hitHists,
                        vector<TH3F*> &lceHists,
                        const size_t noOfHitMaps);



int main(int argc, char* argv[]){


  std::string outfileName = "";


  /***************
   * Check input *
   ***************/
  try{
    if(argc<2) throw "No filename specified";
    string fileName = string(argv[1]);
    outfileName = fileName;
    // check if it is a root file
    fileName = fileName.substr(fileName.length()-5,5);
    if(fileName!=".root") throw "Not a ROOT file";
    // -10 to remove _hits.root from name
    outfileName = outfileName.substr(0,outfileName.length()-10);
    outfileName += "_lce.root";
    TFile* infile = new TFile(argv[1]);
    if(infile->IsZombie()) throw "Error opening file";
    if(!infile->GetListOfKeys()->Contains("startPos"))
      throw "File does not contain the start position histogram";
  }
  catch(const char* msg){
    cerr << msg << " - produceS2LCEMaps will exit" << endl;
    return 1;
  }

  /**************
   * Read input *
   **************/
  TFile* infile = new TFile(argv[1],"read");
  size_t noOfMaps = infile->GetListOfKeys()->GetSize();
  TH3I* startPos = (TH3I*)infile->Get("startPos");
  vector<TH3I*> hitHists;
  int nBinsX = startPos->GetNbinsX();
  int nBinsY = startPos->GetNbinsY();
  int nBinsZ = startPos->GetNbinsZ();
  double xMin = startPos->GetXaxis()->GetBinLowEdge(1);
  double xMax = startPos->GetXaxis()->GetBinUpEdge(nBinsX);
  double yMin = startPos->GetYaxis()->GetBinLowEdge(1);
  double yMax = startPos->GetYaxis()->GetBinUpEdge(nBinsY);
  double zMin = startPos->GetZaxis()->GetBinLowEdge(1);
  double zMax = startPos->GetZaxis()->GetBinUpEdge(nBinsZ);

  TH2I* topCountsHist1 = (TH2I*)infile->Get("topArrayTimeHist1");
  TH2I* topCountsHist2 = (TH2I*)infile->Get("topArrayTimeHist2");
  TH2I* bottomCountsHist1 = (TH2I*)infile->Get("bottomArrayTimeHist1");
  TH2I* bottomCountsHist2 = (TH2I*)infile->Get("bottomArrayTimeHist2");

  /************
   * LCE Maps *
   ************/

  //Create the LCE maps
  std::cout << "creating LCE maps" << std::endl;
  TFile* outfile = new TFile(outfileName.c_str(),"recreate");
  vector<TH3F*> lceHists;
  size_t noOfHitMaps{0};

  for(size_t i = 0; i<noOfMaps; ++i){
    if(i==0){
      hitHists.push_back((TH3I*)infile->Get("allPMTHits"));
      TH3F* newLCEMap = new TH3F("totalHitProb","totalHitProb",
                                 nBinsX,xMin,xMax,
                                 nBinsY,yMin,yMax,
                                 nBinsZ,zMin,zMax);
      lceHists.push_back(newLCEMap);
      ++noOfHitMaps;
    }

    string mapName = string(infile->GetListOfKeys()->At(i)->GetName());

    if(mapName=="startPos" || mapName=="allPMTHits")
      continue;

    else if(mapName.find("PMT") != string::npos){

      hitHists.push_back((TH3I*)infile->Get(mapName.c_str()));
      mapName = mapName.substr(0,mapName.length()-5); // Remove _hits from name
      mapName += "_I";
      TH3F* newLCEMap = new TH3F(mapName.c_str(),mapName.c_str(),
                                 nBinsX,xMin,xMax,
                                 nBinsY,yMin,yMax,
                                 nBinsZ,zMin,zMax);
      lceHists.push_back(newLCEMap);
      ++noOfHitMaps;
    }
  }
  for (size_t i = 0; i<lceHists.size(); ++i){

		lceHists[i]->GetXaxis()->SetTitle("x (mm)");
		lceHists[i]->GetYaxis()->SetTitle("y (mm)");
		lceHists[i]->GetZaxis()->SetTitle("z (mm)");
	}

  //Could do TH3F divide here -> explicitly populate the LCE maps
  //by dividing hits by starting positions

  std::cout << "Populating LCE maps" << std::endl;

  fillLCEHistograms(startPos,
                    hitHists,
                    lceHists,
                    noOfHitMaps);


  /*************
   * Time Maps *
   *************/

  std::cout << "Creating time probability maps" << std::endl;
  // Create copies of time histograms
  // Define probability histograms
  TH2F* topHist1 = new TH2F(*((TH2F*)topCountsHist1));
  TH2F* topHist2 = new TH2F(*((TH2F*)topCountsHist2));
  TH2F* bottomHist1 = new TH2F(*((TH2F*)bottomCountsHist1));
  TH2F* bottomHist2 = new TH2F(*((TH2F*)bottomCountsHist2));
  topHist1->SetName("topPMTs1");
  topHist1->SetTitle("topPMTs1");
  topHist2->SetName("topPMTs2");
  topHist2->SetTitle("topPMTs2");
  bottomHist1->SetName("bottomPMTs1");
  bottomHist1->SetTitle("bottomPMTs1");
  bottomHist2->SetName("bottomPMTs2");
  bottomHist2->SetTitle("bottomPMTs2");

  // Cumulative probability time
  TH2F* cumulativeTopHist1 = new TH2F("cumulativeTopPMTs1","cumulativeTopPMTs1",
                                      topCountsHist1->GetNbinsX(),topCountsHist1->GetXaxis()->GetBinLowEdge(1),topCountsHist1->GetXaxis()->GetBinUpEdge(topCountsHist1->GetNbinsX()),
                                      topCountsHist1->GetNbinsY(),topCountsHist1->GetYaxis()->GetBinLowEdge(1),topCountsHist1->GetYaxis()->GetBinUpEdge(topCountsHist1->GetNbinsY()));
  cumulativeTopHist1->GetXaxis()->SetTitle("r (mm)");
  cumulativeTopHist1->GetYaxis()->SetTitle("time (ns)");

  TH2F* cumulativeTopHist2 = new TH2F("cumulativeTopPMTs2","cumulativeTopPMTs2",
                                      topCountsHist2->GetNbinsX(),topCountsHist2->GetXaxis()->GetBinLowEdge(1),topCountsHist2->GetXaxis()->GetBinUpEdge(topCountsHist2->GetNbinsX()),
                                      topCountsHist2->GetNbinsY(),topCountsHist2->GetYaxis()->GetBinLowEdge(1),topCountsHist2->GetYaxis()->GetBinUpEdge(topCountsHist2->GetNbinsY()));
  cumulativeTopHist2->GetXaxis()->SetTitle("r (mm)");
  cumulativeTopHist2->GetYaxis()->SetTitle("time (ns)");

  TH2F* cumulativeBottomHist1 = new TH2F("cumulativeBottomPMTs1","cumulativeBottomPMTs1",
                                         bottomCountsHist1->GetNbinsX(),bottomCountsHist1->GetXaxis()->GetBinLowEdge(1),bottomCountsHist1->GetXaxis()->GetBinUpEdge(bottomCountsHist1->GetNbinsX()),
                                         bottomCountsHist1->GetNbinsY(),bottomCountsHist1->GetYaxis()->GetBinLowEdge(1),bottomCountsHist1->GetYaxis()->GetBinUpEdge(bottomCountsHist1->GetNbinsY()));
  cumulativeBottomHist1->GetXaxis()->SetTitle("r (mm)");
  cumulativeBottomHist1->GetYaxis()->SetTitle("time (ns)");
  TH2F* cumulativeBottomHist2 = new TH2F("cumulativeBottomPMTs2","cumulativeBottomPMTs2",
                                         bottomCountsHist2->GetNbinsX(),bottomCountsHist2->GetXaxis()->GetBinLowEdge(1),bottomCountsHist2->GetXaxis()->GetBinUpEdge(bottomCountsHist2->GetNbinsX()),
                                         bottomCountsHist2->GetNbinsY(),bottomCountsHist2->GetYaxis()->GetBinLowEdge(1),bottomCountsHist2->GetYaxis()->GetBinUpEdge(bottomCountsHist2->GetNbinsY()));
  cumulativeBottomHist2->GetXaxis()->SetTitle("r (mm)");
  cumulativeBottomHist2->GetYaxis()->SetTitle("time (ns)");

  cumulativeTopHist1->SetName("topArrayTimeHist1");
  cumulativeTopHist1->SetTitle("topArrayTimeHist1");
  cumulativeTopHist2->SetName("topArrayTimeHist2");
  cumulativeTopHist2->SetTitle("topArrayTimeHist2");
  cumulativeBottomHist1->SetName("bottomArrayTimeHist1");
  cumulativeBottomHist1->SetTitle("bottomArrayTimeHist1");
  cumulativeBottomHist2->SetName("bottomArrayTimeHist2");
  cumulativeBottomHist2->SetTitle("bottomArrayTimeHist2");

  // Check fillProbabilityTimeHistogram Function

  // Translate counts into probabilities histograms
  std::cout << "Populating Time Probability Maps" << std::endl;

  fillProbabilityTimeHistograms(cumulativeTopHist1, cumulativeTopHist2,
                                topHist1, topHist2,
                                topCountsHist1, topCountsHist2);

  fillProbabilityTimeHistograms(cumulativeBottomHist1, cumulativeBottomHist2,
                                bottomHist1, bottomHist2,
                                bottomCountsHist1, bottomCountsHist2);


  /******************
   * Write Out Maps *
   ******************/
  // Write LCE Maps
  std::cout << "Write LCE Maps" << std::endl;
  for(size_t i = 0; i<noOfHitMaps; ++i){
    lceHists[i]->Write(lceHists[i]->GetName());
    delete lceHists[i];
  }

  // Write probability Maps
  // std::coud << "Write Probability Maps" << std::endl;
  // topHist1->Write(topHist1->GetName());
  // topHist2->Write(topHist2->GetName());
  // bottomHist1->Write(bottomHist1->GetName());
  // bottomHist2->Write(bottomHist2->GetName());

  // Write cumulative probability Maps
  cumulativeTopHist1->Write(cumulativeTopHist1->GetName());
  cumulativeTopHist2->Write(cumulativeTopHist2->GetName());
  cumulativeBottomHist1->Write(cumulativeBottomHist1->GetName());
  cumulativeBottomHist2->Write(cumulativeBottomHist2->GetName());

  // Delete pointers
  delete topHist1;
  delete topHist2;
  delete bottomHist1;
  delete bottomHist2;
  delete cumulativeTopHist1;
  delete cumulativeTopHist2;
  delete cumulativeBottomHist1;
  delete cumulativeBottomHist2;

  outfile->Close();
  delete outfile;
  return 0;
}


void fillLCEHistograms (TH3I* &startPos,
                        vector<TH3I*> &hitHists,
                        vector<TH3F*> &lceHists,
                        const size_t noOfHitMaps)
{
/**
 * This function changes translates number of hits on a PMT from a starting position into a probability.
 *
 * All inputs contain: photon start positions as xyz
 *
 * \param startPos histogram contains the number of photons which originated from each bin
 * \param hitHists histograms contain the number of photons which hit each PMT from each bin
 * index 0 is the total number of hits from that bin
 * \param lceHists histograms are the histograms which will be filled
 * index 0 is the total hit probability
 *
 **/

  double lce{0};
  int nBins{0};
  double pmtHits{0};

  // Loop over each bin
  for(int i = 1; i<=startPos->GetNbinsX(); ++i){
    for(int j = 1; j<=startPos->GetNbinsY(); ++j){
      for(int k = 1; k<=startPos->GetNbinsZ(); ++k){

        // Get the number of photon hits on PMTs from bin
        double totalHits = hitHists[0]->GetBinContent(i,j,k);

        // Get the number of photons which started in this bin
        double totalStarts = startPos->GetBinContent(i,j,k);

        // Fill total hit probability
        lceHists[0]->SetBinContent(i,j,k,0.0);
        if(totalHits!=0 && totalStarts!=0) {
          lceHists[0]->SetBinContent(i,j,k,totalHits/totalStarts);
          lce += totalHits/totalStarts;
          ++nBins;
        }

        // PMT Light collection efficiency

        pmtHits = 0;

        // Fill top PMTs first
        // As B comes before T (Bottom / Top PMTs)
        // Number of Bottom PMTs = 241
        for(size_t m = 242; m<noOfHitMaps; ++m){

          lceHists[m]->SetBinContent(i,j,k,0.0);
          pmtHits += hitHists[m]->GetBinContent(i,j,k);

//          if (i == 150 && j == 150 && k == 2) {std::cout << pmtHits << std::endl;}

          // If pmt was hit
          if(pmtHits!=0 && totalHits!=0) {
            lceHists[m]->SetBinContent(i, j, k, pmtHits / totalHits);

              if (i == 150 && j == 150 && k == 2) {std::cout << pmtHits / totalHits << std::endl;}
          }
        }

        // Fill Bottom PMTs
        // m = 1 as the first index is the total hit map
        for(size_t m = 1; m<242; ++m){

          lceHists[m]->SetBinContent(i,j,k,0.0);
          pmtHits += hitHists[m]->GetBinContent(i,j,k);

          // If pmt was hit
          if(pmtHits!=0 && totalHits!=0) {
            lceHists[m]->SetBinContent(i, j, k, pmtHits / totalHits);
          }
        }
      } // z bins
    } // y bins
  } // x bins
} // fillLCEHistograms

void fillProbabilityTimeHistograms (TH2F* &cumulativeTimeHist1,
                                    TH2F* &cumulativeTimeHist2,
                                    TH2F* &probTimeHist1,
                                    TH2F* &probTimeHist2,
                                    TH2I* &CountsHist1,
                                    TH2I* &CountsHist2)
{
/**
 * This function changes the number of photon which hit any PMT from the given distance in the given time
 * into a probability.
 *
 * All inputs contain: x = distance (photon originated from PMT it hit)
 *                     y = time (the photon took to hit that PMT)
 *
 * \param cumulativeTimeHist1 histogram to be translated to cumulative probability of hitting a PMT from that distance in any time
 * \param cumulativeTimeHist2 histogram to be translated to cumulative probability of hitting a PMT from that distance in any time
 * \param probTimeHist1 histogram to be translated to the probability of hitting a PMT from that distance in the given time
 * \param probTimeHist2 histogram to be translated to the probability of hitting a PMT from that distance in the given time
 * \param CountsHist1 original histogram
 * \param CountsHist2 original histogram
 *
 **/

  int counts{0}; // number of photons that hit a PMT from this distance
  float probValue{0};
  float cumulativeProbValue{0};

  // Loop over from distances from PMTs bins
  for(size_t i = 1; i<=CountsHist1->GetNbinsX(); ++i){

    // Reset variables
    counts = 0;
    cumulativeProbValue = 0;

    // Loop over time bins to see how many photons hit a PMT from this distance
    for(size_t j = 1; j<=CountsHist1->GetNbinsY(); ++j){
      counts += CountsHist1->GetBinContent(i,j);
    }

    // Loop over the other histogram time bin to see how many photons hit a PMT from this distance
    for(size_t j = 1; j<=CountsHist2->GetNbinsY(); ++j){
      counts += CountsHist2->GetBinContent(i,j);
    }

    // Calculate the probabilities
    // Loop over time bins
    for(int j = 1; j<=CountsHist1->GetNbinsY(); ++j){

      if(counts==0) {
        probValue = 0;
      }
      else {
        // Probability of hitting a PMT from this distance in given time
        probValue = CountsHist1->GetBinContent(i,j)/(float)counts;
      }

      // Add to cumulative probability
      cumulativeProbValue += probValue;

      probTimeHist1->SetBinContent(i,j,probValue);
      cumulativeTimeHist1->SetBinContent(i,j,cumulativeProbValue);
    }

    for(int j = 1; j<=CountsHist2->GetNbinsY(); ++j){

      if(counts==0) {
        probValue = 0;
      }
      else {
        // Probability of hitting a PMT from this distance in given time
        probValue = CountsHist2->GetBinContent(i,j)/(float)counts;
      }

      // Add to cumulative probability
      cumulativeProbValue += probValue;

      probTimeHist2->SetBinContent(i,j,probValue);
      cumulativeTimeHist2->SetBinContent(i,j,cumulativeProbValue);
    }
  } // Distance bins
} //fillProbabliltyTimeHistograms

