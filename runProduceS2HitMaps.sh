#!/bin/bash
#
####################
## SLURM Settings ##
####################
#SBATCH --job-name=S2LightCollection
#SBATCH --array=1-100
#SBATCH --time=6:00:00
#SBATCH --nodes=1
#SBATCH --mem-per-cpu=3G
#SBATCH --ntasks=1 
#SBATCH --account=lz
#SBATCH --output=outputs/logs/slurm_%a.out
#SBATCH --error=outputs/logs/slurm_%a.err
###############################
#
# This script requires the produceS2HitMaps executable to be in the same directory
# In order for this to run, in your SOURCE_DIR you need an outputs directory
# In the outputs directory you need a logs and hits directory
#
# Slurm Notes:
# - memory per cpu has increased from 2GB to 3GB. This reflects the change in BACCARAT release. 3GB is not enough
#   for file hadding on BACCARAT release 4.1.1 with 10 1 million photon histograms.
#
#####################
## Variable Values ##
#####################
# If using slurm
SEED=$SLURM_ARRAY_TASK_ID
# If not using slurm
#SOURCE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
#SEED=100
#
# Oracle OCI
SOURCE_DIR=/mnt/shared/home/sam/S2LightMap
BACC_DIR=/mnt/shared/home/sam/BACCARAT
LZ_BUILD_CONFIG=x86_64-centos7-gcc7-opt
# NERSC
#SOURCE_DIR=/global/projecta/projectdirs/lz/data/bristol/s2maps/sam/S2LightMap
#BACC_DIR=/global/projecta/projectdirs/lz/data/bristol/s2maps/sam/BACCARAT
#BACC_DIR=/mnt/shared/home/sam/BACCARAT
#LZ_BUILD_CONFIG=x86_64-slc6-gcc48-opt
#
# Variables for any implementation
export OUTPUT_DIR=${SOURCE_DIR}/outputs
NPHOTONS=1000000
FILENAME="S2Photons_"
MACRONAME="S2LightCollectionBatch"
#
#
########################
## Setup for BACCARAT ##
########################
BACC_BUILD=${BACC_DIR}/build/${LZ_BUILD_CONFIG}
BACC_EXE=${BACC_BUILD}/bin
source ${BACC_DIR}/setup.sh
#
#
####################
## BACCARAT macro ##
####################
export MACRO_COPY_PATH=${OUTPUT_DIR}/${MACRONAME}_${SEED}.mac
cp ${MACRONAME}.mac ${MACRO_COPY_PATH}
echo "/control/getEnv" "${OUTPUT_DIR}" >> ${MACRO_COPY_PATH}
echo "/Bacc/io/outputDir" "${OUTPUT_DIR}" >> ${MACRO_COPY_PATH}
echo "/Bacc/io/outputName" "${FILENAME}" >> ${MACRO_COPY_PATH}
echo "/Bacc/randomSeed" "${SEED}" >> ${MACRO_COPY_PATH}
echo "/Bacc/beamOn" "${NPHOTONS}" >> ${MACRO_COPY_PATH}
echo "exit" >> ${MACRO_COPY_PATH}
#
#
####################
## Run Simulation ##
####################
echo "Starting BACCARAT S2LightCollection run $SEED"
${BACC_EXE}/BACCARATExecutable ${MACRO_COPY_PATH}
# Remove macro
if [ ${SEED} != 1 ]; then
    rm ${OUTPUT_DIR}/${MACRONAME}_${SEED}.mac
fi
#
#
##############################
## Convert bin to root file ##
##############################
echo "Converting ${FILENAME}${SEED}.bin to root"
${BACC_EXE}/BaccRootConverter ${OUTPUT_DIR}/${FILENAME}${SEED}.bin
# Remove bin file
rm ${OUTPUT_DIR}/${FILENAME}${SEED}.bin
#
#
#########################
## Produce S2 Hit Maps ##
#########################
echo "Producing hits maps in file ${FILENAME}${SEED}_hits.root"
${SOURCE_DIR}/produceS2HitMaps ${OUTPUT_DIR}/${FILENAME}${SEED}.root
# Remove root file
mv ${OUTPUT_DIR}/${FILENAME}${SEED}.root ${OUTPUT_DIR}/roots/${FILENAME}${SEED}.root
# Move hits file
mv ${OUTPUT_DIR}/${FILENAME}${SEED}_hits.root ${OUTPUT_DIR}/hits/${FILENAME}${SEED}_hits.root
