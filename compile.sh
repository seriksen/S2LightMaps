#!/bin/bash
#
# Compile script
#
#############
## For OCI ##
#############
BACC_DIR=/mnt/shared/home/sam/BACCARAT
LZ_BUILD_CONFIG=x86_64-centos7-gcc7-opt
BACC_BUILD=${BACC_DIR}/build/${LZ_BUILD_CONFIG}
#
###############
## For NERSC ##
###############
#BACC_DIR=/global/projecta/projectdirs/lz/data/bristol/s2maps/sam/BACCARAT
#LZ_BUILD_CONFIG=x86_64-slc6-gcc48-opt
#BACC_BUILD=${BACC_DIR}/build/${LZ_BUILD_CONFIG}
#
#####################################
## For pre BACCARAT release 3.16.2 ##
#####################################
#BACC_RELEASE=release-3.16.2
#BACC_DIR=/cvmfs/lz.opensciencegrid.org/BACCARAT/${BACC_RELEASE}
#BACC_BUILD=/cvmfs/lz.opensciencegrid.org/BACCARAT/${BACC_RELEASE}/build.${LZ_BUILD_CONFIG}
#LZ_BUILD_CONFIG=x86_64-slc6-gcc48-opt
#
##########
## ROOT ##
##########
ROOT_DIR=/cvmfs/sft.cern.ch/lcg/releases/LCG_79/ROOT/6.04.02/${LZ_BUILD_CONFIG}
#
source ${BACC_DIR}/setup.sh

echo "Compiling produceS2HitMap"
g++ produceS2HitMaps.cc ${BACC_BUILD}/tools/BaccRootConverterEvent_dict.cxx -o produceS2HitMaps -I${BACC_DIR} -I${BACC_DIR}/tools -I${BACC_BUILD}/tools -I${ROOT_DIR} `root-config --cflags --glibs`

echo "Compiling produceS2LCEMap"
g++ produceS2LCEMaps.cc -o produceS2LCEMaps `root-config --cflags --glibs`
